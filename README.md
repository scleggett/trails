# Trails Microservice

## Running
You can run the app using
```
docker-compose up
docker-compose exec api bundle exec rails db:migrate
```

## Testing

### Smoke/E2E Tests
You can validate the whole stack using cypress

```
cd e2e
npm install
npx cypress open
```
Then click "create_trail_vote.js" in the cypress dialog.

### rspec
You can run rspec through docker via
```
docker-compose exec api bundle exec rails rspec
```

## Todos/Nice-to-haves
- [ ] Add model versioning for endpoints?
- [ ] Add v1 namespace
- [ ] Add rubocop
- [ ] Pagination
- [ ] Randomize values in factories
- [ ] Better data transformations (what to do if event data format doesn't match expected api data?)
- [ ] Better error handling
- [ ] Complete Real Use Flow
  - [ ] Add event producer
  - [ ] Add BFF?
  - [ ] Determine validation (how will we keep from adding mulitple votes?)
  - [ ] Add UI
- [ ] Authentication
- [ ] ...lots more