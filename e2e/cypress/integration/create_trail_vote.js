const { _ } = Cypress

describe('Trail Votes', () => {
    const trailName = `Chimney Gulch Trail ${Date.now()}`;

    describe('Creating a trail vote', () => {
        beforeEach(() => {
            //create vote using rabbitMQ Admin
            cy.visit('http://localhost:15672/#/')
            cy.get('[action="#/login"] [name="username"]').type('guest')
            cy.get('[action="#/login"] [name="password"]').type('guest')
            cy.get('[action="#/login"] [type="submit"]').click()
            cy.get('#tabs [href="#/queues"]').click()
            cy.get('[href="#/queues/%2F/trail_vote_created"]').click()
            cy.contains("Publish message").click()
            cy.get('form[action="#/exchanges/publish"] [name="payload"]').type(
                `{"name":"${trailName}","vote":1,"voter":{"first_name":"Forrest","last_name":"Grant"}}`,
                {parseSpecialCharSequences: false}
            )
            cy.get('form[action="#/exchanges/publish"] [type="submit"]').click();
        });
        it('Processes the vote', () => {
            cy.wait(2) //todo: find a better solution (without this there is a race condition where we create the trail before adding the vote)
            cy.request('http://localhost:3000/trails')
                .then((response) => {
                    expect(response).property('status').to.equal(200);
                    const trail = _.find(response.body, {name: trailName});
                    expect(trail.votes).to.equal(1)
                });
        });

        describe('Adding another vote', () => {
            beforeEach(() => {
                cy.get('form[action="#/exchanges/publish"] [name="payload"]').type(
                    `{"name":"${trailName}","vote":1,"voter":{"first_name":"Forrest","last_name":"Grant"}}`,
                    {parseSpecialCharSequences: false}
                )
                cy.get('form[action="#/exchanges/publish"] [type="submit"]').click();
            });
            it('Increase the vote total', () => {
                cy.request('http://localhost:3000/trails')
                    .then((response) => {
                        expect(response).property('status').to.equal(200);
                        const trail = _.find(response.body, {name: trailName});
                        expect(trail.votes).to.equal(2)
                    });
            });
        });
    })
})