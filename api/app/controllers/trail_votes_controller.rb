class TrailVotesController < ApplicationController
  def create
    # For now just create a trail
    # Unless historical data will be saved in the queue or elsewhere we will probably want to save votes themselves as well.
    # This involves voter lookup though.
    # We could also create a service object here
    # Or create a PORO called ServiceObject
    trail_name, new_trail_votes = trail_vote_params
    @trail = Trail.find_or_create_by!(name: trail_name)
    @trail.votes = @trail.votes + new_trail_votes
    @trail.save!
    render json: {}, status: :created
  end

  private
    def trail_vote_params
      # this was originally params.require(:trail_vote).permit(:name, :vote, :voter)
      # but the message would have to be transformed to get it into that format.
      # I'd probably have another layer in between that converted it,
      # but for now I'm having it accept the attributes directly
      params.require([:name, :vote])
    end
end
