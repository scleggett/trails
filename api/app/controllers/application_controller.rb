class ApplicationController < ActionController::API
  rescue_from StandardError do |exception|
    render json: {errors: exception.message}, status: :internal_server_error
  end
  rescue_from ActionController::ParameterMissing, ActiveRecord::RecordInvalid do |exception|
    render json: {errors: exception.message}, status: :unprocessable_entity
  end
  private
end