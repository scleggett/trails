Rails.application.routes.draw do
  # todo: add v1
  resources :trail_votes, :only => ['create']
  resources :trails, :only => ['index']
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
end
