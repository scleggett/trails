FactoryBot.define do
  factory :trail do
    name { "Turkey Mountain" }
    votes { 1 }
  end
end
