require 'rails_helper'

RSpec.describe "/trail_votes", type: :request do
  let(:valid_attributes) {
    {
      :name => "Atlantis #{Time.current.to_s}",
      :vote => 1,
      :voter => {somekey: "any value"}
    }
  }

  let(:invalid_attributes) {
    {
    }
  }

  let(:valid_headers) {
    {}
  }

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Trail" do
        expect {
          post trail_votes_url,
               params: valid_attributes, headers: valid_headers, as: :json
        }.to change(Trail, :count).by(1)
      end

      it "renders an empty JSON response" do
        post trail_votes_url,
             params: valid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:success)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Trail" do
        expect {
          post trail_votes_url,
               params: invalid_attributes, as: :json
        }.to change(Trail, :count).by(0)
      end

      it "renders a JSON response with errors for the new trail_vote" do
        post trail_votes_url,
             params: { trail_vote: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq("application/json")
      end
    end
  end
end
