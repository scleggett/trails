require 'rails_helper'

RSpec.describe "/trails", type: :request do
  let(:valid_headers) {
    {}
  }

  describe "GET /index" do
    it "renders a successful response" do
      create(:trail)
      get trails_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end
end
