require 'swagger_helper'

RSpec.describe 'trails', type: :request do
  path '/trails' do
    get('list trails') do
      produces 'application/json'
      response(200, 'successful') do
        before do
          create(:trail)
        end
        # todo: this doesn't work for some reason
        # https://github.com/rswag/rswag#enable-auto-generation-examples-from-responses
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
