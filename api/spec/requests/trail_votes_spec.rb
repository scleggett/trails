require 'swagger_helper'

RSpec.describe 'trail_votes', type: :request do

  path '/trail_votes' do

    post('create trail_vote') do
      consumes 'application/json'
      parameter name: :trail_vote, in: :body

      response '201', 'blog created' do
        let(:trail_vote) { { name: 'test trail', vote: 1 } }
        run_test!
      end
    end
  end
end
