class CreateTrails < ActiveRecord::Migration[5.2]
  def change
    create_table :trails do |t|
      t.string :name, :null => false
      t.integer :votes, :null => false, :default => 0

      t.timestamps
    end
  end
end
