#!/usr/bin/env ruby
require 'bunny'
require 'net/http'
require 'yaml'
require 'logger'
require 'json'

queues = YAML.load_file('directory.yml')

log = Logger.new("log.txt")
log.debug(" [*] Opening connections to #{ENV['RABBITMQ_SERVICE_URL']}")

connection = Bunny.new(hostname: ENV['RABBITMQ_SERVICE_URL'])
connection.start
channel = connection.create_channel

queues.each do |queue_name, queue_config|

  queue = channel.queue(queue_name) # todo: look into CRUD conventions for messages
  begin
    log.debug ' [*] Waiting for messages.'
    queue.subscribe(block: true) do |_delivery_info, _properties, body|
      log.debug " [x] Received #{body}"

      # todo: move this into a service for reuse in playback
      url = "http://#{ENV['API_SERVICE_URL']}/#{queue_config['route']}"
      log.debug " [x] Sending to #{url}"
      begin
        res = Net::HTTP.post(
          URI(url),
          body,
          'Content-Type' => 'application/json'
        )
        case res
          when Net::HTTPSuccess
            log.debug " [x] Service Notification Successful"
          else
            log.error  " [x] Service Notification Returned Error: #{res.message}"
        end
      rescue => e
        log.error " [!] ERROR: Unsuccessful request to #{url}"
        log.error e
      end
    end
  rescue Interrupt => _
    connection.close
    exit(0)
  end
end

connection.close
